import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([])
    const fetchData = async () => {

        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
               if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
            console.log(data.hats)

        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    const clickDelete = async (id) => {
        const url = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: "Delete",
        };
        const response = await fetch(url, fetchConfig);
        fetchData()
    }

    return (
        <>
            <div className="mt-3">
                <h1 className="align-bottom px-0 align-start form-check form-check-inline">Hats List</h1>
                <button type="button" className="btn btn-primary mx-0 float-end form-check form-check-inline"><NavLink className="nav-link text-light" to="/hats/new">Submit New Hat</NavLink></button>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Style</th>
                        <th>Color</th>
                        <th>Fabric</th>
                        <th>Location</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        console.log(hat)
                        return (
                            <tr key={hat.id}>
                                <td className="align-middle"><img alt='image of hat' src={hat.picture_url} width="50" /></td>
                                <td className="align-middle">{hat.name}</td>
                                <td className="align-middle">{hat.color}</td>
                                <td className="align-middle">{hat.fabric}</td>
                                <td className="align-middle">Closet {hat.location.closet_name}, Section {hat.location.section_number}, Shelf {hat.location.shelf_number}</td>
                                <td><button className="align-middle btn btn-outline-danger" onClick={() => { clickDelete(hat.id) }}>Delete</button></td>
                            </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </>
    );
}

export default HatsList;
