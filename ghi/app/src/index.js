import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// async function getData() {
//   const resHats = await fetch('http://localhost:8090/api/hats/');

//   Promise.all([resShoes, resHats])
//     .then(values => Promise.all(values.map(value => value.json())))
//     .then(finalVals => {
//       let Shoes = finalVals[0];
//       let Hats = finalVals[1];
//       root.render(
//         <React.StrictMode>
//           <App shoes={Shoes.shoes} hats={Hats.hats} />
//         </React.StrictMode>
//       );
//     })
// }
// getData();
async function loadShoesAndHats() {
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');

  if(shoeResponse.ok && hatsResponse.ok) {
    const shoeData = await shoeResponse.json();
    const hatsData = await hatsResponse.json();
    root.render(
        <App shoes={shoeData.shoes} hats={hatsData.hats} />

    );
  }
  else {
    console.log("ERROR!!!")
  }
}

loadShoesAndHats();
