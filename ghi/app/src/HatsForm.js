import React, { useEffect, useState } from 'react';

function HatsForm() {
    let alertClasses = "alert alert-success d-none";
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        color: '',
        fabric: '',
        picture_url: '',
        location: '',
    })
    const [submitted, setSubmit] = useState('');

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const fetchData = async () => {

        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            console.log("fetched locations")
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log(JSON.stringify(formData))
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setFormData({
                name: '',
                color: '',
                fabric: '',
                picture_url: '',
                location: '',
            })

            setSubmit('submitted!')
        }
    }

    if (submitted.length > 0) {
        alertClasses = "alert alert-success mt-4";
        console.log(alertClasses)
    }
    return (
        <>
            <div className={alertClasses} role="alert">
                Hat successfully submitted!
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Hat Submission!</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input placeholder="Name" onChange={handleFormChange} required value={formData.name} type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="color" onChange={handleFormChange} required value={formData.color} type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="fabric" onChange={handleFormChange} required value={formData.fabric} type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="picture" onChange={handleFormChange} required value={formData.picture} type="text" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select required onChange={handleFormChange} value={formData.location} name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {locations.map(loc => {
                                        return (
                                            <option key={loc.href} value={loc.href}>
                                                {loc.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default HatsForm;
