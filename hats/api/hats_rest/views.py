from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat


class LocationVOEncoder (ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href"
    ]


class HatListEncoder (ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "name",
        "color",
        "picture_url",
        "location"

    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder (ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'name',
        'color',
        'picture_url',

    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"Location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def list_hats(request, location_id=None):
    if request.method == "GET":
        if location_id is not None:
            hats = Hat.objects.filter(location=location_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder = HatListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id "},
                status=400
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def show_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {'message': 'invalid hat id'},
                status=400,
            )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
