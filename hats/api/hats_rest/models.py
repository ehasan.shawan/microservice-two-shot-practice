from django.db import models
from django.urls import reverse




class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, null=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=10)
    picture_url = models.URLField(max_length=400)
    location = models.ForeignKey(LocationVO, related_name="hats", on_delete= models.CASCADE, null=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_hat", kwargs={"id": self.id})
