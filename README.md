# Wardrobify

Team:

* Person 1 - Which microservice?
* Mason Doney - Shoes
* Ehasan Shawan - Hats
* Person 2 - Which microservice?

## Design

## Shoes microservice
Shoes model will have: 
    Manufacturer
    ModelName
    Color
    Url
    BinVO

They will interact by:
    Getting list of bins
    Getting details of bins
    Creating new bins
    Delete Bin
    Update bin

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
